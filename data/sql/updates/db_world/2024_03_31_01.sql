DELETE FROM `gameobject` WHERE `guid` IN (3009344,3009345);
INSERT INTO `gameobject` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`)
VALUES
(3009344,3943,534,0,0,1,1,5525.68,-2818.45,1548.4,4.59894,0,0,-0.746058,0.665882,604800,255,1),
(3009345,3937,534,0,0,1,1,5535.38,-2831.08,1548.4,3.79609,0,0,-0.94693,0.32144,604800,255,1);
